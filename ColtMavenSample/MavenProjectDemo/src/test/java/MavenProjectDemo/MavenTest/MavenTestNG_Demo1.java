package MavenProjectDemo.MavenTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MavenTestNG_Demo1 {
	

	public static String path = System.getProperty("user.dir") +"\\chromedriver_win32\\chromedriver.exe";
	public static WebDriver driver;
	
	
	@BeforeMethod
	public static void setbrowser() {
		System.setProperty("webdriver.chrome.driver", path);
		driver  = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}
	
	@Test(priority=2)
	public static void MyTest() {
		String AppURL = "https://opensource-demo.orangehrmlive.com";
		driver.get(AppURL);
		WebElement UsernameTxb = driver.findElement(By.name("txtUsernameasdfsa"));
		UsernameTxb.isDisplayed();
		System.out.println("My Test got Passed");
	}
	
	@Test(priority=1)
	public static void MyTest1() {
		String AppURL = "https://opensource-demo.orangehrmlive.com";
		driver.get(AppURL);
		WebElement UsernameTxb = driver.findElement(By.name("txtUsername"));
		UsernameTxb.isDisplayed();
		System.out.println("My Test1 got Passed");
	}
	
	@AfterMethod
	public static void closebrowser() {
		driver.close();
	}
	

}
