package MavenProjectDemo.MavenTest;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MavenTest_Demo1 {
	
	public static void loginHRM(WebDriver driver) {	
		
//		Variable Declaration
		String username = "Admin";
		String password = "admin123";
		
//		Login to Orange HRM Page
		String AppURL = "https://opensource-demo.orangehrmlive.com";
		driver.get(AppURL);
		
//		Object Declaration
		WebElement UsernameTxb = driver.findElement(By.name("txtUsername"));
		WebElement PasswordTxb = driver.findElement(By.name("txtPassword"));
		WebElement LoginBtn = driver.findElement(By.id("btnLogin"));
		
		
		UsernameTxb.sendKeys(username);
		PasswordTxb.sendKeys(password);
		LoginBtn.click();
	}
	
	public static void NaigateUserModule(WebDriver driver) {
		
//		Object Declaration
		WebElement AdminLnk = driver.findElement(By.xpath("//b[text()='Admin']//parent::a"));		
		
//		Navigating to Users Module
		AdminLnk.isDisplayed();
		AdminLnk.click();
		WebElement UserManagementLnk = driver.findElement(By.xpath("//a[contains(@id,'UserManagement')]"));		
		UserManagementLnk.click();
		WebElement UsersLnk = driver.findElement(By.xpath("//a[text()='Users']"));
		UsersLnk.isDisplayed();
		UsersLnk.click();
		
		WebElement SystemUsersElem = driver.findElement(By.xpath("//h1[text()='System Users']"));
		SystemUsersElem.isDisplayed();
		System.out.println("System Users Label is Visible");
	}
	
	public static void setUserDetails(WebDriver driver) {
		
//		Variable Declaration
		String UserType = "Admin";
		String EmployeeName = "Cassidy Hope";
		String UserName = "SmartGigAuto";
		String UserStatus = "Enabled";
		String UserPassword = "SmartGigAuto";
		String ConfirmPassword = "SmartGigAuto";
		
		
//		Object Declaration
		WebElement AddBtn = driver.findElement(By.xpath("//input[@id='btnAdd']"));
		
//		SetUserDetails
		AddBtn.isDisplayed();
		AddBtn.click();
		
		WebElement UserTypeLstBx = driver.findElement(By.xpath("//select[@id='systemUser_userType']"));
		WebElement EmployeeNameTxb = driver.findElement(By.xpath("//input[@id='systemUser_employeeName_empName']"));
		WebElement SystemUserNameTxb = driver.findElement(By.xpath("//input[@id='systemUser_userName']"));
		WebElement UserStatusLstBx = driver.findElement(By.xpath("//select[@id='systemUser_status']"));
		WebElement UserPasswordTxb = driver.findElement(By.xpath("//input[@id='systemUser_password']"));
		WebElement UserConfirmPassword = driver.findElement(By.xpath("//input[@id='systemUser_confirmPassword']"));
		WebElement SaveBtn = driver.findElement(By.xpath("//input[@id='btnSave']"));
		
		UserTypeLstBx.isDisplayed();
		List<WebElement> options = UserTypeLstBx.findElements(By.tagName("option"));
		for (WebElement option: options) {
			String CurrentValue = option.getText();
			if (CurrentValue.equals(UserType)) {
				option.click();
				break;
			}
		}
		
		EmployeeNameTxb.sendKeys(EmployeeName);
		SystemUserNameTxb.sendKeys(UserName);
		UserStatusLstBx.isDisplayed();
		options = UserStatusLstBx.findElements(By.tagName("option"));
		for (WebElement option: options) {
			String CurrentValue = option.getText();
			if (CurrentValue.equals(UserStatus)) {
				option.click();
				break;
			}
		}
		
		UserPasswordTxb.sendKeys(UserPassword);
		UserConfirmPassword.sendKeys(ConfirmPassword);
		SaveBtn.click();
		
	}

	public static void main(String[] args) throws InterruptedException, ParseException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") +"\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
//		implicit Wait defenition
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		
		loginHRM(driver);
		
		NaigateUserModule(driver);
		
		setUserDetails(driver);	
		
		driver.close();
		

	}

}
