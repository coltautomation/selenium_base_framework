package com.client.orangehrm.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.client.orangehrm.utils.DriverManager;

public class Orange_HRM_Objects {
	
//	Login to OrangeHRM application
	@FindBy(xpath = "//input[@id='txtUsername']") public WebElement UsernameTxb;
	@FindBy(xpath = "//input[@id='txtPassword']") public WebElement PasswordTxb;
	@FindBy(xpath = "//input[@id='btnLogin']") public WebElement LoginBtn;
	@FindBy(xpath = "//b[text()='Admin']//parent::a") public WebElement AdminLnk;
	
//	Navigate to users module
	@FindBy(xpath = "//a[contains(@id,'UserManagement')]") public WebElement UserManagementLnk;
	@FindBy(xpath = "//a[text()='Users']") public WebElement UsersLnk;
	@FindBy(xpath = "//h1[text()='System Users']") public WebElement SystemUsersElem;
	
//	Add UserDetails	
	@FindBy(xpath = "//input[@id='btnAdd']") public WebElement addBtn;
	@FindBy(xpath = "//select[@id='systemUser_userType']") public WebElement systemUserTypeLsb;
	@FindBy(xpath = "//input[contains(@id,'empName')]") public WebElement empNameTxb;
	@FindBy(xpath = "//input[contains(@id,'userName')]") public WebElement empUserNameTxb;
	@FindBy(xpath = "//select[contains(@id,'User_status')]") public WebElement empStatusLsb;
	@FindBy(xpath = "//input[contains(@id,'User_password')]") public WebElement empPasswordTxb;
	@FindBy(xpath = "//input[contains(@id,'confirmPassword')]") public WebElement empConfirmPasswordTxb;
	@FindBy(xpath = "//input[contains(@id,'btnSave')]") public WebElement saveBtn;
	@FindBy(xpath = "//span[@id='systemUser_password_strength_meter']//parent::div//following-sibling::div//child::span") public WebElement passwordHintElem;
	
	
	
//	Infinity learn
	@FindBy(xpath = "//descendant::button[@type='button' and text()='LOGIN'][2]") public WebElement infinityLoginBtn;
	@FindBy(xpath = "//mat-dialog-container//input[@id='phone']") public WebElement phoneNumbertxb;
	@FindBy(xpath = "//mat-dialog-container//button[text()='NEXT']") public WebElement NextBtn;
	@FindBy(xpath = "//input[@placeholder='First Name']") public WebElement firstNameTxb;
	@FindBy(xpath = "//input[@placeholder='Last Name']") public WebElement lastNameTxb;
	@FindBy(xpath = "//button[text()='REGISTER AND VERIFY NUMBER ']") public WebElement registerVerifyNumberBtn;
	@FindBy(xpath = "//button[text()='LOGIN WITH PASSWORD']") public WebElement loginWithPasswordBtn;
	@FindBy(xpath = "//input[@placeholder='Password']") public WebElement infinityPasswordTxb;
	@FindBy(xpath = "//mat-dialog-container//button[text()='LOGIN']") public WebElement infinityLoginWithPasswordBtn;
	@FindBy(xpath = "//h3[text()='Hello Dheeraj Ch']") public WebElement verifyDheerajLabel;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public Orange_HRM_Objects() {
		WebDriver driver = DriverManager.WEB_DRIVER_THREAD.get();
		PageFactory.initElements(driver, this);
	}

}
