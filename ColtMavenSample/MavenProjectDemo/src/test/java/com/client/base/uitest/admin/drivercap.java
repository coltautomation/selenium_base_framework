package com.client.base.uitest.admin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class drivercap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		
		Map<String, Object> prefs = new HashMap<String, Object>();
		// Set the notification setting it will override the default setting
		prefs.put("profile.default_content_setting_values.notifications", 2);
		prefs.put("profile.default_content_setting_values.popups", 1);
		prefs.put("download.default_directory", System.getProperty("user.dir")+"\\src\\Data\\Downloads");
		
        // Create object of ChromeOption class
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefs);
		options.addArguments("--start-maximized");
		options.addArguments("disable-infobars");
		options.addArguments("--disable-popup-blocking");
		options.addArguments("--force-device-scale-factor=0.75");
		options.addArguments("--disable-dev-shm-usage--");
		options.addArguments("--allow-insecure-localhost");
		options.addArguments("--window-size=1920,1080");
		options.addArguments("enable-automation");
		options.addArguments("--disable-gpu");
		options.addArguments("--no-sandbox");
		options.addArguments("--disable-browser-side-navigation");
		
		HashMap<String, Object> chromeLocalStatePrefs = new HashMap<String, Object>();
		List<String> experimentalFlags = new ArrayList<String>();
		 
		experimentalFlags.add("same-site-by-default-cookies@2");
		experimentalFlags.add("cookies-without-same-site-must-be-secure@1");
		experimentalFlags.add("enable-removing-all-third-party-cookies@2");
		
		chromeLocalStatePrefs.put("browser.enabled_labs_experiments", experimentalFlags);
		options.setExperimentalOption("localState", chromeLocalStatePrefs);
		
		
		capabilities.setCapability(CapabilityType.PAGE_LOAD_STRATEGY, "none");
		//capabilities.setCapability(CapabilityType.l, "none");
		LoggingPreferences logs = new LoggingPreferences(); 
	    logs.enable(LogType.DRIVER, Level.ALL); 
		capabilities.setCapability(CapabilityType.LOGGING_PREFS, logs);
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		capabilities.setCapability("applicationCacheEnabled", false);
		String path = System.getProperty("user.dir")+"\\src\\test\\resources\\downloads\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver",path);
        WebDriver driver = new ChromeDriver(capabilities);

	}

}
