package com.client.config;

public class Config {
	
//	Excel Sheet Mapping
	public static final String TEST_DATA_FILE_PATH = System.getProperty("user.dir")+ "\\TestData\\HRM_testdata.xlsx";
	
//	Application Credentials
	public static final String APP_URL = "https://opensource-demo.orangehrmlive.com";
	public static final String APP_Username = "admin";
	public static final String APP_Password = "admin123";
	
//	Infinity Learn
	public static final String Infinity_APP_URL = "https://infinitylearn.com/#/";
	

}
